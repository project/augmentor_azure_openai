The Azure OpenAI Augmentor is a submodule of Augmentor.

It allows Augmentor to interface with the Azure OpenAI API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/augmentor_azure_openai).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/augmentor_azure_openai).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Augmentor](https://www.drupal.org/project/augmentor)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure the user permissions in Administration » People » Permissions:

   - Administer augmentors

   - Users with this permission will see the webservices > augmentors
     configuration list page. From here they can add, configure, delete, enable
     and disabled augmentors.

   - Warning: Give to trusted roles only; this permission has security
     implications. Allows full administration access to create and edit
     augmentors.


## Maintainers

Current maintainers:

 * Jordan Barnes (https://www.drupal.org/u/j-barnes)
