<?php

namespace Drupal\augmentor_azure_openai;

use Drupal\augmentor\AugmentorBase;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use OpenAI\Client;

/**
 * Provides a base class for Azure OpenAI augmentors.
 *
 * @see \Drupal\augmentor\Annotation\Augmentor
 * @see \Drupal\augmentor\AugmentorInterface
 * @see \Drupal\augmentor\AugmentorManager
 * @see \Drupal\augmentor\AugmentorBase
 * @see plugin_api
 */
class AzureOpenAIBase extends AugmentorBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'base_url' => NULL,
      'api_version' => NULL,
      'prompt' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#default_value' => $this->configuration['base_url'],
      '#description' => $this->t('The base URL that includes both the resource prefix and deployment name. This URL is directly associated with the model, so specifying a model separately is not required.
        Example: {your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}'),
    ];

    $form['api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Version'),
      '#default_value' => $this->configuration['api_version'],
      '#description' => $this->t('The specific version of the Azure OpenAI model to be used.
        Example: 2023-09-15-preview'),
    ];

    $form['prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prompt'),
      '#default_value' => $this->configuration['prompt'] ?? '{input}',
      '#description' => $this->t('The prompt(s) to generate completions for, encoded as a string, array of strings, array of tokens, or array of token arrays.
        Note that <|endoftext|> is the document separator that the model sees during training, so if a prompt is not specified the model will generate as if from the beginning of a new document.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['base_url'] = $form_state->getValue('base_url');
    $this->configuration['api_version'] = $form_state->getValue('api_version');
    $this->configuration['prompt'] = $form_state->getValue('prompt');
  }

  /**
   * Gets the Azure OpenAI API client.
   *
   * @param string $base_url
   *   Base URL for Azure OpenAI API.
   * @param string $api_version
   *   The API version to use.
   *
   * @return \OpenAI\Client
   *   The OpenAI PHP API client.
   */
  public function getClient(string $base_url, string $api_version): Client {
    return \OpenAI::factory()
      ->withBaseUri($base_url)
      ->withHttpHeader('api-key', $this->getKeyValue())
      ->withQueryParam('api-version', $api_version)
      ->make();
  }

  /**
   * Prepares text for prompt inputs.
   *
   * Reused from the OpenAI module.
   * OpenAIs completion endpoint or any other prompt input API
   * performs worse with strings that contain HTML, certain
   * punctuations, whitespace, and newlines.
   *
   * This method will clean up a string before sending it to OpenAI.
   *
   * @param string $text
   *   The text to attach to a prompt.
   * @param array $removeHtmlElements
   *   An array of HTML elements to remove.
   * @param int $max_length
   *   The maximum length of the text to return. A lower limit
   *   will result in faster response from OpenAI and reduce
   *   API usage. A helpful rule of thumb is that one token generally
   *   corresponds to ~4 characters of text for common English text.
   *   This translates to roughly ¾ of a word (so 100 tokens ~= 75 words).
   *
   * @return string
   *   The prepared text.
   */
  public static function prepareText(string $text, array $removeHtmlElements = [], int $max_length = 10000): string {
    // Never include the contents of the following tags.
    $removeHtmlElements += ['pre', 'code', 'script', 'iframe', 'drupal-media'];

    // Ensure we have a root element since LIBXML_HTML_NOIMPLIED is being used.
    // @see https://stackoverflow.com/questions/29493678/loadhtml-libxml-html-noimplied-on-an-html-fragment-generates-incorrect-tags
    $text = '<div>' . $text . '</div>';

    $dom = new \DOMDocument('5.0', 'utf-8');
    $dom->formatOutput = FALSE;
    $dom->preserveWhiteSpace = TRUE;
    $previous = libxml_use_internal_errors(TRUE);
    $dom->loadHTML(mb_encode_numericentity($text, [0x80, 0x10FFFF, 0, ~0], 'UTF-8'));
    $dom->encoding = 'utf-8';
    libxml_clear_errors();
    libxml_use_internal_errors($previous);
    $removeElements = [];

    // Collect a list of DOM nodes we want to remove.
    foreach ($removeHtmlElements as $htmlElement) {
      $tags = $dom->GetElementsByTagName($htmlElement);

      foreach ($tags as $tag) {
        $removeElements[] = $tag;
      }
    }

    // Delete the DOM nodes.
    foreach ($removeElements as $removeElement) {
      $removeElement->parentNode->removeChild($removeElement);
    }

    $text = $dom->saveHTML($dom->documentElement);
    $text = html_entity_decode($text);
    $text = strip_tags(trim($text));
    $text = str_replace(["\r\n", "\r", "\n", "\\r", "\\n", "\\r\\n"], "", $text);
    $text = trim($text);
    $text = preg_replace("/  +/", ' ', $text);
    $text = preg_replace("/[^\w.?!,' ]/iu", '', $text);
    return Unicode::truncate($text, $max_length, TRUE);
  }

}
