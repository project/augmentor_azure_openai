<?php

namespace Drupal\augmentor_azure_openai\Plugin\Augmentor;

use Drupal\augmentor_azure_openai\AzureOpenAIBase;
use Drupal\Core\Form\FormStateInterface;
use OpenAI\Exceptions\TransporterException;

/**
 * Azure OpenAI augmentor plugin implementation.
 *
 * @Augmentor(
 *   id = "azure_openai_completions",
 *   label = @Translation("Azure OpenAI Completions"),
 *   description = @Translation("Given a prompt, the model will return one or
 *    more predicted completions, and can also return the probabilities of
 *    alternative tokens at each position."),
 * )
 */
class AzureOpenAICompletions extends AzureOpenAIBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'temperature' => NULL,
      'max_tokens' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#description' => $this->t('See https://learn.microsoft.com/en-us/azure/ai-services/openai/reference#completions for more information.'),
    ];

    $form['advanced']['temperature'] = [
      '#type' => 'number',
      '#step' => '.01',
      '#title' => $this->t('Temperature'),
      '#default_value' => $this->configuration['temperature'] ?? 0.3,
      '#description' => $this->t('What sampling temperature to use. Higher values means the model will take more risks. Try 0.9 for more creative applications, and 0 (argmax sampling) for ones with a well-defined answer.
        We generally recommend altering this or top_p but not both.'),
    ];

    $form['advanced']['max_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Tokens'),
      '#default_value' => $this->configuration['max_tokens'] ?? 4000,
      '#description' => $this->t("The maximum number of tokens to generate in the completion.
        The token count of your prompt plus max_tokens cannot exceed the model's context length."),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $advance_settings = $form_state->getValue('advanced');
    $this->configuration['temperature'] = $advance_settings['temperature'];
    $this->configuration['max_tokens'] = $advance_settings['max_tokens'];
  }

  /**
   * Creates a completion for the provided prompt and parameters.
   *
   * @param string $input
   *   The text to use as source for the completion manipulation.
   *
   * @return array
   *   The completion output.
   */
  public function execute(string $input): array {
    try {
      $client = $this->getClient($this->configuration['base_url'], $this->configuration['api_version']);
      $preparedInput = $this->prepareText($input);
      $options = $this->buildOptions($preparedInput);
      $result = $client->completions()->create($options);
      return $this->processChoices($result->toArray());
    }
    catch (TransporterException | \Exception $e) {
      $this->logger->error('There was an issue obtaining a response from Azure OpenAI. The error was @error.', ['@error' => $e->getMessage()]);
    }
    return [];
  }

  /**
   * Builds the options array for the completion request.
   *
   * @param string $input
   *   The input string to be used in the options.
   *
   * @return array
   *   The options array for the completion request.
   */
  private function buildOptions(string $input): array {
    return [
      'prompt' => str_replace('{input}', '"' . addslashes($input) . '"', $this->configuration['prompt']),
      'temperature' => (double) $this->configuration['temperature'],
      'max_tokens' => (int) $this->configuration['max_tokens'],
    ];
  }

  /**
   * Processes and normalizes completion choices.
   *
   * @param array $result
   *   The result array from the completion request.
   *
   * @return array
   *   The processed and normalized output array.
   */
  private function processChoices(array $result): array {
    $output = [];

    if (isset($result['choices'])) {
      foreach ($result['choices'] as $choice) {
        $output[] = trim($this->normalizeText($choice['text']), '"');
      }
    }
    return ['default' => $output];
  }

}
